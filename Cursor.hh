<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\orm
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\database\
	{
		common\CommonCursorInterface,
		common\DBOrder,
		common\DBRecord
	};
	use \Iterator;
	
	<<factory>>
	class Cursor implements Iterator<?Model>,CommonCursorInterface
	{
		private CommonCursorInterface $cursor;
		private Model $baseModel;
		//TODO: Cache current()
		// private Vector $cache=Vector{};
		
		public static function getInstance(Model $model,CommonCursorInterface $cursor):Cursor
		{
			$instance=ClassManager::getClassInstance(self::class,$model,$cursor);
			return ($instance instanceof self)?$instance:new self($model,$cursor);
		}
		
		public function __construct(Model $model,CommonCursorInterface $cursor)
		{
			$this->baseModel=$model;
			$this->cursor	=$cursor;
			// $this->next();
		}
		
		public function current():?Model
		{
			$record=$this->cursor->current();
			if (is_null($record))
			{
				return null;
			}
			$baseModel=$this->baseModel;
			if ($record instanceof DynamicModel)
			{
				return $record;
			}
			invariant($record instanceof Map, 'must be a Map or DynamicModel.');
			if (!$baseModel instanceof DynamicModel)
			{
				return $baseModel::create($record);
			}
			else
			{
				return ORM::create($baseModel->getCollection(),$record);
			}
		}
		
		public function key():mixed
		{
			return $this->cursor->key();
		}
		
		public function next():void
		{
			$this->cursor->next();
		}
		
		public function rewind():void
		{
			$this->cursor->rewind();
		}
		
		public function valid():bool
		{
			return !is_null($this->current());
		}
		
		public function seek(int $position):void
		{
			$this->cursor->seek($position);
		}
		
		public function count():int
		{
			return $this->cursor->count();
		}
		
		public function __call(string $method, array<mixed> $args):mixed
		{
			return call_user_func_array([$this->cursor,$method],$args);
		}
		
		public function limit(int $limit):this
		{
			// $this->cursor->limit($limit);
			return $this;
		}
		
		public function offset(int $offset):this
		{
			// $this->cursor->offset($offset);
			return $this;
		}
		
		public function orderBy(DBOrder $order):this
		{
			
			return $this;
		}
	}
}
