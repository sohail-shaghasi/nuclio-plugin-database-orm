<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\orm
{
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	use nuclio\plugin\database\
	{
		datasource\source\Source,
		common\DBQuery,
		common\DBFields,
		common\DBRecord,
		common\DBOptions
	};
	
	<<singleton>>
	class ORM extends Plugin
	{
		private ?Source $boundDataSource=null;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):ORM
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public static function create(string $collection,?Map<string,mixed> $record=null):DynamicModel
		{
			$instance=new DynamicModel($record);
			$instance->setCollection($collection);
			$instance->flagDirty();
			return $instance;
		}
		
		public static function find(string $collection, DBQuery $query, DBOptions $options=Map{}):Cursor
		{
			$ORM=ORM::getInstance();
			$instance=new DynamicModel();
			$instance->setCollection($collection);
			return $ORM->findWithModel($instance,$query,$options);
		}
		
		public static function findOne(string $collection, DBQuery $query, DBOptions $options=Map{}):?Model
		{
			$ORM=ORM::getInstance();
			$instance=new DynamicModel();
			$instance->setCollection($collection);
			return $ORM->findOneWithModel($instance,$query,$options);
		}
		
		public static function findById(string $collection, mixed $id, DBOptions $options=Map{}):?Model
		{
			$ORM=ORM::getInstance();
			$instance=new DynamicModel();
			$instance->setCollection($collection);
			return $ORM->findByIdWithModel($instance,$id,$options);
		}
		
		public static function delete(string $collection, DBQuery $query):bool
		{
			$ORM=ORM::getInstance();
			$instance=new DynamicModel();
			$instance->setCollection($collection);
			return $ORM->deleteWithModel($instance,$query);
		}
		
		public static function distinct(string $collection, string $field):bool
		{
			$ORM=ORM::getInstance();
			$instance=new DynamicModel();
			$instance->setCollection($collection);
			return $ORM->distinctWithModel($instance,$query);
		}
		
		public function bindDataSource(Source $dataSource):void
		{
			$this->boundDataSource=$dataSource;
		}
		
		public function getDataSource():Source
		{
			return $this->boundDataSource;
		}
		
		public function save(Model $model):DBRecord
		{
			if (!is_null($this->boundDataSource))
			{
				return $model->toArrayWithRelations()
				|> new Map($$)
				|> $this->boundDataSource->upsert($model->getCollection(),$$);
			}
			else
			{
				throw new ORMException('Unable to save model. No Data Source has been bound to the ORM.');
			}
		}
		
		public function findWithModel(Model $model, DBQuery $query, DBOptions $options=Map{}):Cursor
		{
			if (!is_null($this->boundDataSource))
			{
				$records=$this->boundDataSource->find
				(
					$model->getCollection(),
					$query,
					$options
				);
				$cursor=Cursor::getInstance($model,$records);
				return $cursor;
			}
			else
			{
				throw new ORMException('Unable to find records. No Data Source has been bound to the ORM.');
			}
		}
		
		public function findOneWithModel(Model $model, DBQuery $query, DBOptions $options=Map{}):?Model
		{
			if (!is_null($this->boundDataSource))
			{
				$record=$this->boundDataSource->findOne
				(
					$model->getCollection(),
					$query,
					$options
				);
				if (!is_null($record))
				{
					if (!$model instanceof DynamicModel)
					{
						return $model::create($record);
					}
					else
					{
						return ORM::create($model->getCollection(),$record);
					}
				}
				return null;
			}
			else
			{
				throw new ORMException('Unable to find record. No Data Source has been bound to the ORM.');
			}
		}
		
		public function findByIdWithModel(Model $model, mixed $id, DBOptions $options=Map{}):?Model
		{
			if (!is_null($this->boundDataSource))
			{
				$record=$this->boundDataSource->findById($model->getCollection(),$id,$options);
				if (!is_null($record))
				{
					if (!$model instanceof DynamicModel)
					{
						return $model::create($record);
					}
					else
					{
						return ORM::create($model->getCollection(),$record);
					}
				}
				return null;
			}
			else
			{
				throw new ORMException('Unable to find record. No Data Source has been bound to the ORM.');
			}
		}
		
		public function deleteWithModel(Model $model,DBQuery $query):bool
		{
			if (!is_null($this->boundDataSource))
			{
				return $this->boundDataSource->delete($model->getCollection(),$query);
			}
			else
			{
				throw new ORMException('Unable to find record. No Data Source has been bound to the ORM.');
			}
		}
		
		public function distinctWithModel(Model $model,string $field):Vector<mixed>
		{
			if (!is_null($this->boundDataSource))
			{
				return $this->boundDataSource->distinct($model->getCollection(),$field);
			}
			else
			{
				throw new ORMException('Unable to find record. No Data Source has been bound to the ORM.');
			}
		}
	}
}
