<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\orm
{
	use nuclio\plugin\
	{
		database\orm\ORM,
		annotations\reader\Reader
	};
	
	<<__ConsistentConstruct>>
	class DynamicModel extends Model
	{
		public static function create(?string $collection=null,?Map<string,mixed> $record=null):this
		{
			$instance=new static($record);
			if (!is_null($collection))
			{
				$instance->setCollection($collection);
			}
			else
			{
				$annotationReader	=Reader::getInstance(static::class);
				$annotations		=$annotationReader->getAnnotations();
				$class=$annotations->get('class');
				if (!is_null($class))
				{
					$instance->setCollection((string)$class->get('collection'));
				}
				else
				{
					throw new ORMException('No collection asigned to model.');
				}
			}
			$instance->flagDirty();
			return $instance;
		}
		
		public function __construct(/* HH_FIXME[4033] */...$args)
		{
			$this->_ORM			=ORM::getInstance();
			$this->_fields		=Vector{};
			$this->_idFields	=Vector{};
			$annotationReader	=Reader::getInstance($this);
			$this->_annotations	=$annotationReader->getAnnotations();
			$args				=new Vector($args);
			$this->mapIDFields();
			
			if ($this->_idFields->count()===0)
			{
				$this->_idFields[]='_id';
			}
			if ($args->get(0) instanceof Map)
			{
				foreach($args->get(0) as $field => $fieldValue)
				{
					$this->saveField($field);
				}
				$this->setMany($args[0]);
			}
			$this->flagClean();
		}
		
		public function setCollection(string $collection):this
		{
			$this->_collection=$collection;
			return $this;
		}
		
		public function set(string $key,mixed $val):Model
		{
			$this->saveField($key);
			return parent::set($key,$val);
		}
		
		public function __call(string $method,array<mixed> $args):mixed
		{
			$args=new Vector($args);
			$matches=[];
			if (preg_match('/(set|get|delete|add|remove|push|pull)(.*)/',$method,$matches))
			{
				if (substr($matches[1],0,3)==='set')
				{
					if (!$args->containsKey(0))
					{
						$args=new Vector(null);
					}
					$key=$matches[2];
					
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					
					$this->saveField($key);
					
					if ($this->isIdField($key))
					{
						return $this->set($key.'Id',$args[0]);
					}
					else
					{
						return $this->set($key,$args[0]);
					}
				}
				else if (substr($matches[1],0,3)==='get')
				{
					$key=$matches[2];
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					return $this->get($key);
				}
				else if (in_array(substr($matches[1],0,4),['push','add']))
				{
					if (!$args->containsKey(0))
					{
						$args=new Vector(null);
					}
					$key=$matches[2];
					
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					
					if (property_exists($this,$key))
					{
						$container=$args[0];
						if (!$args[0] instanceof Vector)
						{
							$container=Vector{$container};
						}
						if ($container instanceof Vector)
						{
							for ($i=0,$j=count($container[0]); $i<$j; $i++)
							{
								$this->push($key,$container[$i]);
							}
						}
						return $this;
					}
					else
					{
						$key.='s';
						if (property_exists($this,$key))
						{
							return $this->push($key,$args[0]);
						}
					}
				}
			}
			throw new ORMException('The method "'.$method.'" on model "'.__CLASS__.'" does not exist and could not be automagically overloaded.');
		}
	}
}
